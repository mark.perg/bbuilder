import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-burder.firebaseio.com/'
});

export default instance;