import axios from 'axios';
import * as actionTypes from '../actions/actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START,
    };
};

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token, 
        userId: userId
    };
};

export const authFailed = (error) => {
    return {
        type: actionTypes.AUTH_FAILED,
        error: error
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
}

export const checkAuthTimeout = (experationTime) => {
    return dispatch => {
        setTimeout( () => {
            dispatch(logout());
        }, experationTime * 1000)
    };
};

export const auth = (email, pass, isSignup) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: pass,
            returnSecureToken: true
        }
        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAAC5ttanCmq1EIMX0gFwLlnm0DAITOaxY';
        if(!isSignup){
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAAC5ttanCmq1EIMX0gFwLlnm0DAITOaxY';
        }
        axios.post(url, authData)
        .then(resp => {
            const expirationDate = new Date(new Date().getTime() + resp.data.expiresIn * 1000);
            localStorage.setItem('token', resp.data.idToken);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', resp.data.localId);
            dispatch(authSuccess(resp.data.idToken, resp.data.localId));
            dispatch(checkAuthTimeout(resp.data.expiresIn));
        })
        .catch(err => {
            dispatch(authFailed(err.response.data.error.message));
        })
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token){
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if(expirationDate <= new Date()){
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}

