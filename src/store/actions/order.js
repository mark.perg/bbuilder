import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const purchaseOrderSuccess = (order_id, orderData) => {
    return {
        type: actionTypes.PURCHASE_ORDER_SUCCESS,
        id: order_id,
        data: orderData
    };
};

export const purchaseOrderFailed = (error) => {
    return {
        type: actionTypes.PURCHASE_ORDER_FAILED,
        error: error
    };
};

export const purchaseOrderStart = () => {
    return {
        type: actionTypes.PURCHASE_ORDER_START,
    };
};


export const purchaseOrder = (orderData, token) => {

    return dispatch => {
        dispatch(purchaseOrderStart());
        axios.post( '/orders.json?auth=' + token, orderData )
        .then( response => {
            console.log(response.data)
            dispatch(purchaseOrderSuccess(response.data.name, orderData))
        } )
        .catch( error => {
            dispatch(purchaseOrderFailed(error))
        } );
    };
};

export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT
    }
};

export const fetchOrders = (token, userId) => {
    return dispatch => {
        dispatch(fetchOrdersStart());
        const queryParams = '?auth=' + token + '&orderBy="userId"&equalsTo="' + userId + '"';
        axios.get('/orders.json' + queryParams)
        .then(res => {
            const fetchedOrders = [];
            for (let key in res.data) {
                fetchedOrders.push({
                    ...res.data[key],
                    id: key
                });
            }
            dispatch(fetchOrdersSuccess(fetchedOrders));
        })
        .catch(err => {
            dispatch(fetchOrdersFailed(err));
        });
    }
}

export const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START
    }
}

export const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    }
}

export const fetchOrdersFailed = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILED,
        error: error
    }
}